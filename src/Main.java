import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukkan nama karyawan tetap: ");
        String namaTetap = scanner.nextLine();
        System.out.print("Masukkan gaji pokok karyawan tetap: ");
        int gajiPokokTetap = scanner.nextInt();
        System.out.print("Masukkan tunjangan anak karyawan tetap: ");
        int tunjanganAnakTetap = scanner.nextInt();
        scanner.nextLine();

        System.out.print("Masukkan nama karyawan kontrak: ");
        String namaKontrak = scanner.nextLine();
        System.out.print("Masukkan upah harian karyawan kontrak: ");
        int upahHarianKontrak = scanner.nextInt();
        System.out.print("Masukkan jumlah hari masuk karyawan kontrak: ");
        int jumlahHariMasukKontrak = scanner.nextInt();
        System.out.print("Masukkan tunjangan anak karyawan kontrak: ");
        int tunjanganAnakKontrak = scanner.nextInt();

        KaryawanTetap karyawanTetap = new KaryawanTetap(namaTetap, gajiPokokTetap, tunjanganAnakTetap);
        KaryawanKontrak karyawanKontrak = new KaryawanKontrak(namaKontrak, upahHarianKontrak, jumlahHariMasukKontrak, tunjanganAnakKontrak);

        System.out.println("Total gaji karyawan tetap: " + karyawanTetap.hitungTotalGaji());
        System.out.println("Total upah karyawan kontrak: " + karyawanKontrak.hitungTotalUpah());
    }
}