import java.util.Scanner;

class KaryawanTetap {
    String nama;
    int gajiPokok;
    int tunjanganAnak;

    KaryawanTetap(String nama, int gajiPokok, int tunjanganAnak) {
        this.nama = nama;
        this.gajiPokok = gajiPokok;
        this.tunjanganAnak = tunjanganAnak;
    }

    int hitungTotalGaji() {
        return gajiPokok + tunjanganAnak;
    }
}
