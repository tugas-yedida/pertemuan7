class KaryawanKontrak {
    String nama;
    int upahHarian;
    int jumlahHariMasuk;
    int tunjanganAnak;

    KaryawanKontrak(String nama, int upahHarian, int jumlahHariMasuk, int tunjanganAnak) {
        this.nama = nama;
        this.upahHarian = upahHarian;
        this.jumlahHariMasuk = jumlahHariMasuk;
        this.tunjanganAnak = tunjanganAnak;
    }

    int hitungTotalUpah() {
        return (upahHarian * jumlahHariMasuk) + tunjanganAnak;
    }
}